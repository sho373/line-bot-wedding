const functions = require("firebase-functions");
const admin = require("firebase-admin");
const path = require("path");
const os = require("os");
const fs = require("fs");
const axios = require("axios");
const spawn = require("child-process-promise").spawn;
const express = require("express");
const line = require("@line/bot-sdk");
const region = "asia-northeast1";

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();
const projectName = functions.config().wedding.projectname
const config = {
  channelSecret: functions.config().wedding.channelsecret,
  channelAccessToken: functions.config().wedding.accesstoken,
};
const getRandomInt = (max) => {
  return Math.floor(Math.random() * max);
};
const saveImage = async (id) => {
  const response = await axios({
    method: "get",
    responseType: "arraybuffer",
    url: `https://api-data.line.me/v2/bot/message/${id}/content`,
    headers: {
      Authorization: `Bearer ${config.channelAccessToken}`,
      "Content-Type": "image/png",
    },
  });
  functions.logger.log("Hello from info", response.data);
  const fileName = `${id}.png`;
  const tempFilePath = path.join(os.tmpdir(), fileName);
  const bucket = admin.storage().bucket(projectName);

  await fs.writeFile(tempFilePath, response.data, function (err, result) {
    if (err) functions.logger.log("error", err);
  });

  const metadata = {
    contentType: "image/png",
  };
  await bucket.upload(tempFilePath, {
    destination: `${id}.png`,
    metadata: metadata,
  });
  functions.logger.log("upload done");
  const newDocRef = db.collection("images").doc();

  await newDocRef
    .set({
      createdAt: new Date(),
      visible: true,
      docid: newDocRef.id,
      itemid: id,
      url: `https://firebasestorage.googleapis.com/v0/b/${projectName}/o/${fileName}?alt=media`,
    })
    .then(() => {})
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  await fs.unlinkSync(tempFilePath);
};

const saveMovie = async (id) => {
  const response = await axios({
    method: "get",
    responseType: "arraybuffer",
    url: `https://api-data.line.me/v2/bot/message/${id}/content`,
    headers: {
      Authorization: `Bearer ${config.channelAccessToken}`,
      "Content-Type": "video/mp4",
    },
  });
  functions.logger.log("Hello from info. Here's an object:", response.data);
  const fileName = `${id}.mp4`;
  const tempFilePath = path.join(os.tmpdir(), fileName);
  const bucket = admin.storage().bucket(projectName);

  await fs.writeFile(tempFilePath, response.data, function (err, result) {
    if (err) functions.logger.log("error", err);
  });
  functions.logger.log("temp movie save done");

  const metadata = {
    contentType: "video/mp4",
  };
  await bucket.upload(tempFilePath, {
    destination: `${id}.mp4`,
    metadata: metadata,
  });
  functions.logger.log("upload movie done");
  const newDocRef = db.collection("videos").doc();

  await newDocRef
    .set({
      createdAt: new Date(),
      visible: true,
      docid: newDocRef.id,
      itemid: id,
      thumnail: `https://firebasestorage.googleapis.com/v0/b/${projectName}/o/thumb_${fileName}.png?alt=media`,
      url: `https://firebasestorage.googleapis.com/v0/b/${projectName}/o/${fileName}?alt=media`,
    })
    .then(() => {})
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  await fs.unlinkSync(tempFilePath);
};

const app = express();

app.post("/webhook", line.middleware(config), (req, res) => {
  Promise.all(req.body.events.map(handleEvent)).then((result) =>
    res.json(result)
  );
});

const client = new line.Client(config);

async function handleEvent(event) {
  if (event.message.type === "text") {
    return client.replyMessage(event.replyToken, {
      type: "text",
      text: "メッセージありがとうございます。写真、動画を送信すると下記ウェブサイトにて共有することができます。", //実際に返信の言葉を入れる箇所
    });
  }
  if (event.message.type === "image") {
    let message;
    let n = getRandomInt(5);
    switch (n) {
      case 0:
        message = "素敵な写真ありがとうございます。";
        break;
      case 1:
        message = "写真のシェアありがとうございます。";
        break;
      case 2:
        message = "写真ありがとうございます。";
        break;
      case 3:
        message = "ありがとうございます。";
        break;
      case 4:
        message = "写真のシェアありがとうございます。";
        break;
      default:
        message = "送信ありがとうございます。";
    }
    await saveImage(event.message.id);
    return client.replyMessage(event.replyToken, {
      type: "text",
      text: message,
    });
  }
  if (event.message.type === "video") {
    await saveMovie(event.message.id);
    return client.replyMessage(event.replyToken, {
      type: "text",
      text: "動画のシェアありがとうございます。投稿した動画、写真は下記ウェブサイトでご覧いただけます。", //実際に返信の言葉を入れる箇所
    });
  }
}

exports.app = functions.region(region).https.onRequest(app);

exports.createVideoThumb = functions
  .region(region)
  .storage // .runWith({timeoutSeconds:300,memory:'512MB'})
  .object()
  .onFinalize(async (object) => {
    const fileBucket = object.bucket;
    const filePath = object.name;
    const contentType = object.contentType;
    const dir = path.dirname(filePath);
    const fileName = path.basename(filePath);

    functions.logger.log(fileBucket, filePath, contentType, dir, fileName);

    // if this is not an image stop execution
    if (!contentType.startsWith("video/")) return console.log("Not an video");

    if (fileName.startsWith("thumb_")) {
      return console.log("Video is already a thumbnail");
    }

    // download file to memory
    const bucket = admin.storage().bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    const metadata = {
      contentType: "image/png",
    };

    // download the file from bucket to tmp disk
    try {
      const upRes = await bucket
        .file(filePath)
        .download({ destination: tempFilePath });
      console.log("UP_RES", JSON.stringify(upRes));
      functions.logger.log("video has been downloaded to :", tempFilePath);
    } catch (error) {
      console.log("err at download bucket", error);
    }

    const thmbFileName = `thumb_${fileName}.png`;
    const locatThmbFilePath = path.join(os.tmpdir(), thmbFileName);
    const remoteThmbFilePath = path.join(dir, thmbFileName);

    await spawn("ffmpeg", [
      "-i",
      tempFilePath,
      "-vframes",
      "1",
      "-an",
      "-s",
      "400X400",
      "-ss",
      "1",
      locatThmbFilePath,
    ]);
    functions.logger.log("Thumbnail has been created");

    await bucket.upload(locatThmbFilePath, {
      destination: remoteThmbFilePath,
      metadata: metadata,
      public: true,
    });

    functions.logger.log("Thumbmain uploaded to the bucket");

    // return 'Task done';
    await fs.unlinkSync(locatThmbFilePath);
    return fs.unlinkSync(tempFilePath);
  });
