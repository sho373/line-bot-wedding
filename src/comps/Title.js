import React from "react";
import "./title.styles.scss";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
    margin: theme.spacing(1),
  },
}));

const Title = (props) => {
  const classes = useStyles();
  return (
    <div className="title">
      {/* <h1>Weddingram</h1> */}
      <img
        alt="logo"
        width="140"
        height="30"
        src={process.env.REACT_APP_LOGO}
      />
      <h2>{props.value === 1 ? "Wedding Videos" : "Wedding Pictures"}</h2>
      <Avatar
        className={classes.avatar}
        alt="avatar"
        src={process.env.REACT_APP_DEFAULT_PROFILE}
      />
      <p>
        本日はご多忙のところ結婚式にご出席いただき誠にありがとうございます。ゲストの皆様が撮った写真、動画をこのサイトで見ることができます。
      </p>
    </div>
  );
};

export default Title;
