import React from "react";
import useFirestore from "../../../hooks/useFirestore";
import { motion } from "framer-motion";
import "./image.grid.styles.scss";

const ImageGrid = ({ setSelectedImg, setSelectedIndex }) => {
  const { docs } = useFirestore("images");

  return (
    <div className="img-grid">
      {docs &&
        docs.map((doc, id) => (
          <div
            className="img-wrap"
            key={doc.id}
            onClick={() => {
              setSelectedImg(doc.url);
              setSelectedIndex(id);
            }}
          >
            <motion.img
              src={doc.url}
              alt="uploaded pic"
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ delay: 1 }}
            />
          </div>
        ))}
    </div>
  );
};

export default ImageGrid;
