import React, { useState } from "react";
import { motion } from "framer-motion";
import { projectFirestore } from "../../../firebase/config";
import useFirestore from "../../../hooks/useFirestore";
import Swipeable from 'react-swipeable'
import ReactSwipeEvents from 'react-swipe-events'
import "./modal.styles.scss";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { FaEllipsisV, FaChevronRight, FaChevronLeft } from "react-icons/fa";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";

const Modal = ({
  setSelectedImg,
  selectedImg,
  setSelectedIndex,
  selectedIndex,
}) => {
  const { docs } = useFirestore("images");
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [dialogOpen, setDialogOpen] = useState(false);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const [index, setIndex] = useState(selectedIndex);
  const handleClick = (e) => {
    if (e.target.classList.contains("backdrop")) {
      setSelectedImg(null);
    }
  };
  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };
  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  const hideImage = (id) => {
    setDialogOpen(false);
    setMobileMoreAnchorEl(null);
    projectFirestore
      .collection("images")
      .where("itemid", "==", id)
      .get()
      .then((response) => {
        if (response.size === 1) {
          response.docs.forEach((doc) => {
            projectFirestore
              .collection("images")
              .doc(doc.id)
              .update({
                visible: false,
              })
              .then(() => {
                setSelectedImg(null);
              });
          });
        }
      });
  };
  const nextImage = (num) => {
    if (num < docs.length - 1) {
      setSelectedImg(docs[num + 1].url);
      setIndex(index + 1);
    }
  };
  const prevImage = (num) => {
    if (num !== 0) {
      setSelectedImg(docs[num - 1].url);
      setIndex(index - 1);
    }
  };

  return (
    <>
    
      <motion.div
        className="backdrop"
        onClick={handleClick}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
      >
        <FaEllipsisV onClick={handleMobileMenuOpen} className="more" />
        <ReactSwipeEvents
          onSwipedRight={() => prevImage(index)}
          onSwipedLeft={() => nextImage(index)}>
        <motion.img
          src={selectedImg}
          alt="enlarged pic"
          initial={{ y: "-100vh" }}
          animate={{ y: 0 }}
        />
        </ReactSwipeEvents>
        <FaChevronRight className="next" onClick={() => nextImage(index)} />
        <FaChevronLeft className="prev" onClick={() => prevImage(index)} />
      </motion.div>

      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        id={"mobileMenuId"}
        keepMounted
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}
      >
        <MenuItem
          onClick={() => {
            setDialogOpen(true);
          }}
        >
          この写真を非表示にする
        </MenuItem>
        <MenuItem
          onClick={() => {
            setSelectedImg(null);
          }}
        >
          閉じる
        </MenuItem>
      </Menu>
      {dialogOpen && (
        <Dialog
          open={dialogOpen}
          onClose={handleDialogClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle>本当にこの写真を非表示にしますか？</DialogTitle>

          <DialogActions>
            <Button
              variant="outlined"
              onClick={handleDialogClose}
              color="default"
            >
              キャンセル
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() =>
                hideImage(selectedImg.split("/o/").pop().split(".png")[0])
              }
            >
              非表示にする
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
};

export default Modal;
