import React from "react";
import useFirestore from "../../../hooks/useFirestore";
import { motion } from "framer-motion";
import "./video.grid.styles.scss";

const VideoGrid = ({ setSelectedImg }) => {
  const { docs } = useFirestore("videos");

  return (
    <div className="video-grid">
      {docs &&
        docs.map((doc) => (
          <div
            className="video-wrap"
            key={doc.id}
            onClick={() => setSelectedImg(doc.url)}
          >
            <motion.img
              src={doc.thumnail}
              alt="uploaded pic"
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ delay: 1 }}
            />
          </div>
        ))}
    </div>
  );
};

export default VideoGrid;
