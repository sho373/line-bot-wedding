import React, { useState } from "react";
import { projectFirestore } from "../../../firebase/config";
import "./video.modal.styles.scss";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import { FaEllipsisV } from "react-icons/fa";
import ReactPlayer from "react-player/lazy";

const VideoModal = ({ setSelectedImg, selectedImg }) => {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [dialogOpen, setDialogOpen] = useState(false);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleClick = (e) => {
    if (e.target.classList.contains("backdrop")) {
      setSelectedImg(null);
    }
  };
  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };
  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  const hideMovie = (id) => {
    setDialogOpen(false);
    setMobileMoreAnchorEl(null);
    projectFirestore
      .collection("videos")
      .where("itemid", "==", id)
      .get()
      .then((response) => {
        if (response.size === 1) {
          response.docs.forEach((doc) => {
            projectFirestore
              .collection("videos")
              .doc(doc.id)
              .update({
                visible: false,
              })
              .then(() => {
                setSelectedImg(null);
              });
          });
        }
      });
  };
  return (
    <>
      <div className="backdrop" onClick={handleClick}>
        <FaEllipsisV onClick={handleMobileMenuOpen} className="more" />
        <ReactPlayer width="100%" height="100%" controls url={selectedImg} />
      </div>
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        id={"mobileMenuId"}
        keepMounted
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}
      >
        <MenuItem
          onClick={() => {
            setDialogOpen(true);
          }}
        >
          この動画を非表示にする
        </MenuItem>
        <MenuItem
          onClick={() => {
            setSelectedImg(null);
          }}
        >
          閉じる
        </MenuItem>
      </Menu>
      {dialogOpen && (
        <Dialog
          open={dialogOpen}
          onClose={handleDialogClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle>本当にこの動画を非表示にしますか？</DialogTitle>

          <DialogActions>
            <Button
              variant="outlined"
              onClick={handleDialogClose}
              color="default"
            >
              キャンセル
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() =>
                hideMovie(selectedImg.split("/o/").pop().split(".mp4")[0])
              }
            >
              非表示にする
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
};

export default VideoModal;
