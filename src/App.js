import React, { useState } from "react";
import Title from "./comps/Title";
import PropTypes from "prop-types";
import ImageGrid from "./comps/Images/grid/ImageGrid";
import VideoGrid from "./comps/videos/grid/videoGrid";
import Modal from "./comps/Images/modal/Modal";
import VideoModal from "./comps/videos/modal/videoModal";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import { FaVideo, FaGripHorizontal } from "react-icons/fa";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={0}>
          <Typography component={"span"}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appbar: {
    backgroundColor: "#fff",
    marginBottom: theme.spacing(2),
  },
  icon: {
    color: "#808080",
    fontSize: 20,
  },
}));

function App() {
  const [selectedImg, setSelectedImg] = useState(null);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="App">
      <Title value={value} />
      <AppBar elevation={0} position="static" className={classes.appbar}>
        <Tabs
          value={value}
          onChange={handleChange}
          variant="fullWidth"
          aria-label="simple tabs example"
          indicatorColor="secondary"
        >
          <Tab
            icon={<FaGripHorizontal className={classes.icon} />}
            aria-label="images"
            {...a11yProps(0)}
          />
          <Tab
            icon={<FaVideo className={classes.icon} />}
            aria-label="movies"
            {...a11yProps(1)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ImageGrid
          setSelectedImg={setSelectedImg}
          setSelectedIndex={setSelectedIndex}
        />
        {selectedImg && (
          <Modal
            selectedImg={selectedImg}
            setSelectedImg={setSelectedImg}
            selectedIndex={selectedIndex}
            setSelectedIndex={setSelectedIndex}
          />
        )}
      </TabPanel>
      <TabPanel value={value} index={1}>
        <VideoGrid setSelectedImg={setSelectedImg} />
        {selectedImg && (
          <VideoModal
            selectedImg={selectedImg}
            setSelectedImg={setSelectedImg}
          />
        )}
      </TabPanel>
    </div>
  );
}

export default App;
